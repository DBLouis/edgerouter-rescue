#!/usr/bin/env bash

function md5 {
	md5sum "$1" | cut -d ' ' -f 1
}

if [ $# -ne 2 ]
then
	echo "Usage: $0 <image> <device>"
	exit 1
fi

img="$1"

if [ ! -f "${img}" ]
then
	echo 'Image file does not exist!'
	exit 1
fi

dev="$2"
dev_boot="${dev}1"
dev_root="${dev}2"

if [ "${UID}" -ne 0 ]
then
	sudo='sudo'
fi

if [ ! -b "${dev}" ]
then
	echo "${dev} is not a block device!"
	exit 1
fi

tmp="$(mktemp -d)" || { echo 'Failed to create temporary directory!'; exit 1; }

mnt_boot="${tmp}/boot"
mnt_root="${tmp}/root"

kernel_orig="${tmp}/vmlinux.tmp"
kernel_orig_md5="${tmp}/vmlinux.tmp.md5"
squashfs_orig="${tmp}/squashfs.tmp"
squashfs_orig_md5="${tmp}/squashfs.tmp.md5"
version_orig="${tmp}/version.tmp"

kernel="${mnt_boot}/vmlinux.64"
kernel_md5="${mnt_boot}/vmlinux.64.md5"
squashfs="${mnt_root}/squashfs.img"
squashfs_md5="${mnt_root}/squashfs.img.md5"
version="${mnt_root}/version"

${sudo} umount "${dev_boot}" >/dev/null 2>&1
${sudo} umount "${dev_root}" >/dev/null 2>&1

${sudo} mkdir "${mnt_boot}"
${sudo} mkdir "${mnt_root}"

${sudo} /sbin/parted --script "${dev}" mktable msdos

${sudo} /sbin/parted --script "${dev}" mkpart primary fat32 1 150MB
${sudo} mkfs.vfat "${dev_boot}"

${sudo} /sbin/parted --script "${dev}" mkpart primary ext3 150MB 100%
${sudo} mkfs.ext3 -q "${dev_root}"

${sudo} mount "${dev_boot}" "${mnt_boot}"
${sudo} mount "${dev_root}" "${mnt_root}"

${sudo} tar -xf "${img}" -C "${tmp}" || { echo 'Extraction failed!'; exit 1; }

if [ $(md5 "${kernel_orig}") != "$(cat "${kernel_orig_md5}")" ]
then
	echo 'Kernel image corrupted!'
	exit 1
fi

${sudo} cp "${kernel_orig}" "${kernel}"
${sudo} cp "${kernel_orig_md5}" "${kernel_md5}"

if [ $(md5 "${squashfs_orig}") != "$(cat "${squashfs_orig_md5}")" ]
then
	echo 'System image corrupted!'
	exit 1
fi

${sudo} cp "${squashfs_orig}" "${squashfs}"
${sudo} cp "${squashfs_orig_md5}" "${squashfs_md5}"

${sudo} cp "${version_orig}" "${version}"

${sudo} mkdir "${mnt_root}/w"

sync

${sudo} umount "${mnt_boot}"
${sudo} umount "${mnt_root}"

${sudo} rm -rf "${tmp}"

exit 0
